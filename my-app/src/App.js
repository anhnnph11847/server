//import reset from 'react-style-reset';
import QuestionDetail from "./components/QuestionDetail";
import QuestionDetail2 from "./components/QuestionDetail2";
import styled, { createGlobalStyle } from "styled-components";
import { BrowserRouter, Route, Link } from "react-router-dom";
import AnswerQ from "./components/AnswerQ";
import AnswerQ2 from "./components/AnswerQ2";
import Questionlist from "./components/QuestionList";
import Ask from "./components/Ask";
import socketIOClient from "socket.io-client";
import { useState, useEffect } from "react";
const GlobalStyled = createGlobalStyle`
  body{
    background : #dfdfdf;
    color: #262626;
  }
`;
const Styleheader = styled.div`
  font-size: 1.8rem;
`;

// const hearderRow = styled.div`
//     display: grid;
//     grid-template-colums: 1fr min-content;
//     padding: 30px 20px;
// `;
function App() {
  localStorage.setItem("anh", "123");
  const [valuse, setValuse] = useState("");
  return (
    <div className="App">
      <reset />
      <GlobalStyled />
      <BrowserRouter>
        <hearderRow>
          <Styleheader>
            <Link to="/">Fpoly Q&A</Link>
          </Styleheader>
          <Styleheader>
            <Link to="/questionlist">Cau hoi</Link>
          </Styleheader>
        </hearderRow>
        <Route path="/questionlist">
          <Questionlist setValuse={setValuse} valuse={valuse} />
        </Route>
        <Route path="/questiondetail">
          <QuestionDetail valuse={valuse} />
        </Route>
        {/* <Route path='/questiondetail2'>
          <QuestionDetail2 />
        </Route>
        <Route path='/answerQ'>
          <AnswerQ />
        </Route>
        <Route path='/answerQ2'>
          <AnswerQ2 />
        </Route>
        <Route path='/ask'>
          <Ask />
        </Route> */}
      </BrowserRouter>
      <BrowserRouter
        getUserConfirmation={(message, callback) => {
          // this is the default behavior
          console.log("da");
          const allowTransition = window.confirm(message);
          callback(allowTransition);
        }}
      />
    </div>
  );
}

export default App;
