import React from "react";
//import { Link, Redirect } from "react-router-dom";
import styled from "styled-components";
import QuestionDetail from "./QuestionDetail";
//import Questionlist from "./QuestionList";
import socketIOClient from "socket.io-client";
import { useState, useEffect } from "react";
const Main = styled.div`
  display: grid;
  magin-top: 10px;
`;
const Answer = styled.div`
  float: center;
  magin-top: 10px;
`;

const TextAnser = styled.textarea`
  resize: none;
  overflow: auto;
  height: 300px;
  width: 300px;
`;

export default function AnswerQ() {
  const [response, setResponse] = useState("");
  const [traloi1, setTraloi1] = useState("a");
  const onchan = (event) => {
    event.preventDefault();
    setResponse(event.target.value);
  };
  useEffect(() => {
    const socket = socketIOClient.connect("http://localhost:4001/");
    const dlgui = {
      use: "anh",
      room: 123,
    };
    socket.emit("dn", dlgui);
    socket.open();
    socket.on("connection", (dl) => {
      console.log(socket.connected);
      console.log(dl);
    });
    socket.on("message1", (dl1) => {
      setTraloi1(dl1);
      console.log(dl1);
    });
  });
  const traloi = function (event) {
    event.preventDefault();
    const socket = socketIOClient("http://localhost:4001/");
    console.log(response);
    const dlguia = {
      text: response,
      room: "null",
    };
    socket.emit("chatto", dlguia);
  };

  return (
    <Main>
      <QuestionDetail  traloi1={traloi1} />
      <hr></hr>
      <Answer>
        <form>
          <label>
            <TextAnser
              placeholder="Nhap cau tra loi vao day"
              name="answer"
              onChange={(event) => {
                onchan(event);
              }}
            ></TextAnser>
          </label>
          <button
            type="submit"
            onClick={(event) => {
              traloi(event);
            }}
          >
            Gui tra loi
          </button>
        </form>
      </Answer>
    </Main>
  );
}
