import React from "react";
//import { Link } from "react-router-dom";
import styled from "styled-components";
import socketIOClient from "socket.io-client";
import { useState, useEffect } from "react";
import axios from "axios";
//import { useState } from "react";

// const QuestionText = styled.div`
// `;

const QuestionRow = styled.div`
  font-size: 1.5rem;
`;
// const Main = styled.div`
//   display: grid;
//   magin-top: 10px;
// `;
const Answer = styled.div`
  float: center;
  magin-top: 10px;
`;
const Text = styled.textarea`
  resize: none;
  overflow: auto;
  height: 50px;
  width: 300px;
`;
export default function QuestionDetail({valuse}) {
console.log({valuse});
if(localStorage.getItem("roomnow")!== valuse.id){
  socketIOClient("http://localhost:4001/").emit("leaveroom",{
    roombefore:localStorage.getItem("roomnow"),
    roomnow: valuse.id
  });
  localStorage.setItem("roomnow", valuse.id);
}
  // console.log(localStorage.getItem("anh"));
  const [response, setResponse] = useState("");
  const [categories, setCategories] = useState([]);
  const onchan = (event) => {
    event.preventDefault();
    setResponse(event.target.value);
  };
  useEffect(() => {
    const socket = socketIOClient.connect("http://localhost:4001/");
    socket.emit("leaveroom",{
        roombefore:localStorage.getItem("roomnow"),
        roomnow:valuse.id
      });
      localStorage.setItem("roomnow",valuse.id);
      socket.open();
      if( localStorage.getItem("tokencall")===null){
        socket.on("connection", (dl) => {
          let token = socket.handshake.query.token;
          console.log(socket.connected);
          console.log(token);
          localStorage.setItem("tokencall",token)
        });
      }
    socket.on("message1", (dl1) => {
      outPutMessage(dl1);
    });
  }, []);
  const traloi = function (event) {
    event.preventDefault();
    const socket = socketIOClient("http://localhost:4001/");
    const dlguia = {
      text: response,
      room: valuse.id,
    };
    socket.emit("Comment", dlguia);
  };
  return (
    <main>
      <QuestionRow>
        <div>{valuse.title}</div>
        <div>{valuse.conten}</div>
        <br />
        <div>
          <div class="chat-messages"></div>
          {categories.map((valu, index) => {
            return (
              <>
               <h3> {valu.conten} </h3>
              </>
            );
          })}
        </div>
        {/* <Link to="/answerQ"> tra loi</Link> */}
        <Answer>
          <form>
            <label>
              <Text
                placeholder="Nhap cau tra loi vao day"
                name="answer"
                onChange={(event) => {
                  onchan(event);
                }}
              ></Text>
            </label>
            <button
              type="submit"
              onClick={(event) => {
                traloi(event);
              }}
            >
              Gui tra loi
            </button>
          </form>
        </Answer>
      </QuestionRow>
    </main>
  );
}
function outPutMessage(dl1) {
  const div = document.createElement("div");
  div.innerHTML = `
                  ${dl1}
                        
  `;
  document.querySelector(".chat-messages").appendChild(div);
  console.log(div);
}
