import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { userData } from "../dl";
import socketIOClient from "socket.io-client";
import { useState, useEffect } from "react";
//import { BrowserRouter, Switch, Route } from "react-router-dom";
//import QuestionDetail from './QuestionDetail';

// const Styleheader = styled.div`
//     font-size: 1.8rem;
// `;
// const hearderRow = styled.div`
// magin-botton: 10px;
//     display: grid;
//     grid-template-colums: 1fr min-content;
//     padding: 30px 20px;
// `;

const QuestionRow = styled.div`
  background-color: rgb(167, 167, 167);
`;

const QuestionStat = styled.span`
  magin-top: 10px;
  display: inline-block;
  magin-right: 5px;
  background-color: #3e4a52;
  color: #9cc3db;
  font-size: 1.1rem;
  padding: 4px;
`;

const QuestionTile = styled.div`
  color: #3ca4ff;
  font-size: 1.3rem;
`;
//  display: block; text-decoration: none;
const ButtonAsk = styled.button`
    display: inline-block;
    background-color: #378ad3;
    color: #fff;
    border-radius: 5px;
    border: 0;
    padding 7px;
    magin-left:3px;
`;
const Text = styled.textarea`
  resize: none;
  overflow: auto;
  height: 50px;
  width: 300px;
`;
const Answer = styled.div`
  float: center;
  magin-top: 10px;
`;
function Questionlist({ setValuse }) {
  const room=0;
  useEffect(() => {
    const socket = socketIOClient.connect("http://localhost:4001/");
    socket.emit("leaveroom",{
      roombefore:localStorage.getItem("roomnow"),
      roomnow:room
    });
    socket.open();
  //  if( localStorage.getItem("tokencall")===null){
      socket.on("connection", (dl) => {
        let token = socket.handshake.query.token;
        console.log(socket.connected);
        console.log(token);
        localStorage.setItem("tokencall",token)
      });
  //  }
    socket.on("message1", (dl1) => {
      outPutMessage(dl1);
    });
  }, []);
 
  const click = (valu) => {
    // const socket = socketIOClient.connect("http://localhost:4001/");
    // socket.emit("joinRoom", "1");
    setValuse(valu);
  };
  const [response, setResponse] = useState("");
  const onchan = (event) => {
    event.preventDefault();
    setResponse(event.target.value);
  };
  const traloi = function (event) {
    event.preventDefault();
    const socket = socketIOClient("http://localhost:4001/");
    const dlguia = {
      text: response,
      room: 0,
    };
    socket.emit("NewBlog", dlguia);
  };
  
 
  return (
    <>
      <main>
        <hearderRow>
          <ButtonAsk>
            <Link to="/ask">Dat cau hoi</Link>{" "}
          </ButtonAsk>
          <div class="chat-messages">
          {userData.map((valu, index) => {
            return (
              <>
                <hr></hr>
                <QuestionRow>
                  <QuestionStat>
                    <span>{valu.title}</span>
                  </QuestionStat>
                  <QuestionTile>
                    <Link to="/questiondetail">
                      {" "}
                      <h2 onClick={() => click(valu)}>{valu.conten}</h2>
                    </Link>
                  </QuestionTile>
                </QuestionRow>
              </>
            );
          })}
       </div>
          <Answer>
            <form>
              <label>
                <Text
                  placeholder="Nhap cau tra loi vao day"
                  name="answer"
                  onChange={(event) => {
                    onchan(event);
                  }}
                ></Text>
              </label>
              <button
                type="submit"
                onClick={(event) => {
                  traloi(event);
                }}
              >
                Dang bài
              </button>
            </form>
          </Answer>
          {/* <hr></hr> */}
          {/* <QuestionRow>
            <QuestionStat>
              <span>Toan</span>
            </QuestionStat>
            <QuestionTile>
              <Link to="/questiondetail">
                {" "}
                <h2>1 + 1 = ?</h2>
              </Link>
            </QuestionTile>
          </QuestionRow>
          <QuestionRow>
            <QuestionStat>Tieng Anh</QuestionStat>
            <QuestionTile>
              <Link to="/questiondetail2">
                <h2>
                  I. Combine the following sentences by using “to Infinitive /
                  so as to / in order to” to express purpose.
                </h2>
              </Link>
            </QuestionTile>
          </QuestionRow> */}
        </hearderRow>
      </main>
    </>
  );
}
export default Questionlist;
function outPutMessage(dl1) {
  const div = document.createElement("div");
  div.innerHTML = `
    ${dl1}
  `;
  document.querySelector(".chat-messages").appendChild(div);
}
